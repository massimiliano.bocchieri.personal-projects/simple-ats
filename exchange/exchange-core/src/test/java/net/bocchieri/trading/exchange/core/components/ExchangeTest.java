package net.bocchieri.trading.exchange.core.components;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.javadsl.TestKit;
import net.bocchieri.trading.exchange.api.messages.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static net.bocchieri.trading.exchange.core.TestSetup.*;

public class ExchangeTest {
  private static final OrderData SAMPLE_ORDER_DATA = new OrderData(1, "AA", "Buy", 1, 2, 1);
  private static ActorSystem system;
  
  @BeforeClass
  public static void setup() {
    system = ActorSystem.create();
  }
  
  @AfterClass
  public static void teardown() {
    TestKit.shutdownActorSystem(system);
    system = null;
  }
  
  @Test
  public void testExchangeFlowFromSubscriptionToOrderData() {
    final TestKit subscriberTestProbe = makeTestProbe(system);
    final TestKit routerTestProbe = makeTestProbe(system);
    
    final ActorRef exchange = system.actorOf(Props.create(Exchange.class));
    
    exchange.tell(new Subscription(1), subscriberTestProbe.getRef());
    
    subscriberTestProbe.expectMsgClass(longWait(), MarketData.class);
    
    exchange.tell(SAMPLE_ORDER_DATA, routerTestProbe.getRef());
    
    routerTestProbe.expectNoMessage(longWait());
  }
  
  @Test
  public void testUnsubscriptionForUnregisteredSubscriber() {
    final TestKit subscriberTestProbe = makeTestProbe(system);
    
    final ActorRef exchange = system.actorOf(Props.create(Exchange.class));
    
    exchange.tell(new UnSubscription(1), subscriberTestProbe.getRef());
    
    subscriberTestProbe.expectMsgClass(longWait(), NotSubscribed.class);
  }
  
  @Test
  public void testAlreadyRegisteredSubscriber() {
    final TestKit subscriberTestProbe = makeTestProbe(system);
    final TestKit alreadyRegisterSubscriberTestProbe = makeTestProbe(system);
    
    final ActorRef exchange = system.actorOf(Props.create(Exchange.class));
    
    exchange.tell(new Subscription(1), subscriberTestProbe.getRef());
    
    subscriberTestProbe.expectMsgClass(longWait(), MarketData.class);
    
    exchange.tell(new Subscription(1), alreadyRegisterSubscriberTestProbe.getRef());
    
    alreadyRegisterSubscriberTestProbe.expectMsgClass(longWait(), AlreadySubscribed.class);
  }
  
  @Test
  public void testUnhandled() {
    final TestKit subscriberTestProbe = makeTestProbe(system);
    final ActorRef exchange = system.actorOf(Props.create(Exchange.class));
    
    exchange.tell(new Object(), subscriberTestProbe.getRef());
    
    subscriberTestProbe.expectNoMessage(shortWait());
  }
  
}