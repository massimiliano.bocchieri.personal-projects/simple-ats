package net.bocchieri.trading.exchange.core;

import akka.actor.ActorSystem;
import akka.testkit.javadsl.TestKit;

import java.time.Duration;

public class TestSetup {
  public static TestKit makeTestProbe(ActorSystem system) {
    return new TestKit(system);
  }
  
  public static Duration shortWait() {
    return Duration.ofMillis(20);
  }
  
  public static Duration longWait() {
    return Duration.ofMillis(50);
  }
}
