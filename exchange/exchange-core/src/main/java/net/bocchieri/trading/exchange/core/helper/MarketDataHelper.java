package net.bocchieri.trading.exchange.core.helper;

import net.bocchieri.trading.exchange.api.messages.MarketData;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

public class MarketDataHelper {
  private static final java.util.Random Random = new Random();
  
  private static final String[] SYMBOLS = {"GOOGL", "AMZN", "NFLX", "TWTR", "ION"};
  
  public static MarketData buildRandomMarketDataItem() {
    return new MarketData(SYMBOLS[Random.nextInt(SYMBOLS.length)], roundDouble(Random.nextDouble() * 100, 2),
            roundDouble(Random.nextDouble() * 100, 2), Random.nextInt(1000), Random.nextInt(1000));
  }
  
  private static double roundDouble(final double value, final int scale) {
    return new BigDecimal(value).setScale(scale, RoundingMode.HALF_UP).doubleValue();
  }
}
