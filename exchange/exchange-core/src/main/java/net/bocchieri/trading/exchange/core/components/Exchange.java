package net.bocchieri.trading.exchange.core.components;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import net.bocchieri.trading.exchange.api.messages.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Exchange extends AbstractActor {
  private final LoggingAdapter logger = Logging.getLogger(getContext().system(), this);
  
  private final Map<Long, ActorRef> subscribers;
  private final Map<ActorRef, Thread> streamers;
  
  public Exchange() {
    subscribers = new HashMap<>();
    streamers = new HashMap<>();
  }
  
  @Override
  public void preStart() throws Exception {
    super.preStart();
    
    logger.info(String.format("Exchange opened on %s", new Date()));
  }
  
  @Override
  public Receive createReceive() {
    return receiveBuilder()
                   .match(Subscription.class, subscription -> addSubscriber(subscription.subscriberId, getSender()))
                   .match(UnSubscription.class, unsubscription -> removeSubscriber(unsubscription.subscriberId, getSender()))
                   .match(OrderData.class, orderData -> reveivedOrderdata(orderData, getSender()))
                   .build();
  }
  
  @Override
  public void postStop() throws Exception {
    super.postStop();
    
    logger.info(String.format("Exchange is closing on %s", new Date()));
    
    shutDownActiveMarketDataStreamers();
    
    notifyAndShutDownSubscribers();
    logger.info(String.format("Exchange closed on %s", new Date()));
  }
  
  private void addSubscriber(long subscriberId, ActorRef subscriber) {
    logger.info(String.format("Received Subscription Request from <%s> related to subscriber with  id <%d>", subscriber.toString(), subscriberId));
    
    if (!subscribers.containsKey(subscriberId))
      registerNewSubscriber(subscriberId, subscriber);
    else
      subscriber.tell(new AlreadySubscribed(subscriberId), self());
  }
  
  private void removeSubscriber(long subscriberId, ActorRef subscriber) {
    logger.info(String.format("Received Unsbscription Request from <%s> related to subscriber with  id <%d>", subscriber.toString(), subscriberId));
    
    if (subscribers.containsKey(subscriberId))
      unregisterSubscriber(subscriberId, subscriber);
    else
      subscriber.tell(new NotSubscribed(subscriberId), self());
  }
  
  private void reveivedOrderdata(OrderData orderData, ActorRef subscriberRouter) {
    logger.info(String.format("Received OrderData with id <%d>  by <%s> on behalf of subscriber with  id <%d>",
            orderData.id, subscriberRouter.toString(), orderData.subscriberId));
    
    if (subscribers.containsKey(orderData.subscriberId)) {
      handleOrder(orderData);
    } else
      subscriberRouter.tell(new NotSubscribed(orderData.subscriberId), self());
  }
  
  private void handleOrder(OrderData orderData) {
    logger.info(String.format("Accepted Order with id <%d>  related to subscriber with  id <%d>",
            orderData.id, orderData.subscriberId));
    
    final ActorRef subscriber = subscribers.get(orderData.subscriberId);
    
    subscriber.tell(orderData, self());
    
    logger.info(String.format("Notified  Order <%d> Acceptance to subscriber <%s>  with  id <%d>",
            orderData.id, subscriber.toString(), orderData.subscriberId));
  }
  
  private void registerNewSubscriber(long subscriberId, ActorRef subscriber) {
    subscribers.put(subscriberId, subscriber);
    streamSubscriber(subscriber);
  }
  
  private void unregisterSubscriber(long subscriberId, ActorRef subscriber) {
    stopStreaming(subscriber);
    subscribers.remove(subscriberId);
  }
  
  private void streamSubscriber(ActorRef subscriber) {
    logger.debug(String.format("Started streaming  market data to subscriber <%s>", subscriber.toString()));
    
    final Thread streamer = new Thread(new MarketDataStreamer(subscriber, self()));
    streamers.put(subscriber, streamer);
    streamer.start();
  }
  
  private void stopStreaming(ActorRef subscriber) {
    final Thread streamer = streamers.get(subscriber);
    if (streamer != null) {
      if (streamer.isAlive())
        streamer.interrupt();
      
      streamers.remove(subscriber);
    }
    
    logger.debug(String.format("Stopped streaming Subscriber <%s>", subscriber.toString()));
  }
  
  private void notifyAndShutDownSubscribers() {
    logger.debug(String.format("Unregistering  %d Market Data subscribers", subscribers.size()));
    
    final Date now = new Date();
    
    for (ActorRef subscriber : subscribers.values()) {
      subscriber.tell(new ExchangeClosed(now), self());
      context().stop(subscriber);
    }
  }
  
  private void shutDownActiveMarketDataStreamers() {
    logger.debug(String.format("Shutting down %d Market Data streamers", streamers.size()));
    
    for (Thread streamer : streamers.values())
      if (streamer.isAlive())
        streamer.interrupt();
  }
}
