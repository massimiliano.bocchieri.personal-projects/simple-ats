package net.bocchieri.trading.exchange.core.components;

import akka.actor.ActorRef;

import static java.lang.Thread.sleep;
import static net.bocchieri.trading.exchange.core.helper.MarketDataHelper.buildRandomMarketDataItem;


public class MarketDataStreamer implements Runnable {
  private static final int SLEEP_MILLISECONDS = 1000;
  private static final int SLEEP_ITERATION_BLOCK = 10;
  private final ActorRef recipient;
  private final ActorRef exchange;
  
  MarketDataStreamer(ActorRef recipient, ActorRef exchange) {
    this.recipient = recipient;
    this.exchange = exchange;
  }
  
  @Override
  public void run() {
    
    try {
      for (int i = 1; ; i++) {
        if (i % SLEEP_ITERATION_BLOCK == 0) {
          sleep(SLEEP_MILLISECONDS);
          i = 1;
        } else {
          recipient.tell(buildRandomMarketDataItem(), exchange);
        }
      }
    } catch (InterruptedException ignored) {
      //Interrupted by Controlled explicit Exchange request
    }
  }
}
