package net.bocchieri.trading.exchange.api.messages;

import java.io.Serializable;

public final class UnSubscription implements Serializable {
  private static final long serialVersionUID = 1L;
  
  public final long subscriberId;
  
  public UnSubscription(long subscriberId) {
    this.subscriberId = subscriberId;
  }
}
