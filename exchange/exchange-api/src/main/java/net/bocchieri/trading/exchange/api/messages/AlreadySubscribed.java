package net.bocchieri.trading.exchange.api.messages;

import java.io.Serializable;

public class AlreadySubscribed implements Serializable {
  public static final long serialVersionUID = 1L;
  
  public final long subscriberId;
  
  public AlreadySubscribed(final long subscriberId) {
    this.subscriberId = subscriberId;
  }
}
