package net.bocchieri.trading.exchange.api.messages;

import java.io.Serializable;
import java.util.Date;

public class ExchangeClosed implements Serializable {
  public static final long serialVersionUID = 1L;
  
  public final Date closingDate;
  
  public ExchangeClosed(final Date closingDate) {
    this.closingDate = closingDate;
  }
}
