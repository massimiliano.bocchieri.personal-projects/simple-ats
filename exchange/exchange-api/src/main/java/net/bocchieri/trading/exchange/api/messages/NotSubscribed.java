package net.bocchieri.trading.exchange.api.messages;

import java.io.Serializable;

public class NotSubscribed implements Serializable {
  public static final long serialVersionUID = 1L;
  
  public final long subscriberId;
  
  public NotSubscribed(final long subscriberId) {
    this.subscriberId = subscriberId;
  }
}
