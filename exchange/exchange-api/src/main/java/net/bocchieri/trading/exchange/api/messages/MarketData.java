package net.bocchieri.trading.exchange.api.messages;

import java.io.Serializable;
import java.util.Date;

public final class MarketData implements Serializable {
  private static final long serialVersionUID = 1L;
  private static int nextID = 1;
  public final String symbol;
  public final double bid;
  public final double ask;
  public final int bidSize;
  public final int askSize;
  public final Date quoteDateTime;
  public volatile long id;
  
  public MarketData(final String symbol, final double bid, final double ask, final int bidSize, final int askSize) {
    this.id = nextID++;
    this.quoteDateTime = new Date();
    
    this.symbol = symbol;
    this.bid = bid;
    this.ask = ask;
    this.bidSize = bidSize;
    this.askSize = askSize;
  }
  
  @Override
  public String toString() {
    return String.format("MarketData [id = %d, symbol=%s, bid=%s, ask=%s, bidSize=%d, askSize=%d, quoteTime=%s]", id, symbol, bid, ask, bidSize, askSize, quoteDateTime);
  }
}