package net.bocchieri.trading.exchange.api.messages;

import java.io.Serializable;

public final class OrderData implements Serializable {
  public static final long serialVersionUID = 1L;
  
  public final long id;
  public final String symbol;
  public final double price;
  public final int quantity;
  public final long subscriberId;
  public String orderSide;
  
  public OrderData(long id, String symbol, String orderSide, double price, int quantity, long subscriberId) {
    this.id = id;
    this.symbol = symbol;
    this.orderSide = orderSide;
    this.price = price;
    this.quantity = quantity;
    this.subscriberId = subscriberId;
  }
  
  @Override
  public String toString() {
    return String.format("OrderData{id=%d, symbol='%s', orderSide='%s', price=%s, quantity=%d, subscriberId=%d}", id, symbol, orderSide, price, quantity, subscriberId);
  }
}
