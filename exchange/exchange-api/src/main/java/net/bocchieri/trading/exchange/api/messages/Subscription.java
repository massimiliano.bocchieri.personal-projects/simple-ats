package net.bocchieri.trading.exchange.api.messages;

import java.io.Serializable;

public final class Subscription implements Serializable {
  public static final long serialVersionUID = 1L;
  
  public final long subscriberId;
  
  public Subscription(long subscriberId) {
    this.subscriberId = subscriberId;
  }
}
