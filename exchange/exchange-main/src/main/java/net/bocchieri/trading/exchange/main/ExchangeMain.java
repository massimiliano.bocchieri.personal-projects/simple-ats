package net.bocchieri.trading.exchange.main;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.typesafe.config.ConfigFactory;
import net.bocchieri.trading.exchange.core.components.Exchange;

import java.io.IOException;

public class ExchangeMain {
  private static final String REMOTE_EXCHANGE_SYSTEM_ID = "RemoteExchangeSystem";
  private static final String EXCHANGE_ID = "Exchange";
  
  private ActorSystem system;
  private ActorRef exchange;
  
  public static void main(String[] args) {
    ExchangeMain main = new ExchangeMain();
    
    main.initSystem();
    main.setUpExchange();
    
    main.handleExecutionFlow();
  }
  
  private void initSystem() {
    system = ActorSystem.create(REMOTE_EXCHANGE_SYSTEM_ID, ConfigFactory.load());
  }
  
  private void setUpExchange() {
    exchange = system.actorOf(Props.create(Exchange.class), EXCHANGE_ID);
  }
  
  private void handleExecutionFlow() {
    System.out.println(">>> Press ENTER to Shut Down Exchange System or type Ctrl-C to  abort execution...<<<");
    try {
      System.in.read();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      stopExchange();
      system.terminate();
    }
  }
  
  private void stopExchange() {
    system.stop(exchange);
  }
}
