#### Simple ATS - Algorithmic Trading System Architecture based on Actor Model [Akka toolkit](https://akka.io) / version 1.0.0

###### Brief Description

Simple ATS is an experiment built while trying to understand the **"unbounded"** context of Algorithmic Trading System architecture.
The direction of the project is toward the architecture building components rather than its algorithmic strategies.

###### Project Structure, Components (Akka Actors), Responsibilities and Execution Flow

The project is composed of two "self running" modules: **exchange**  and **ats**:

- **exchange** represents the Exchange supplying Level 2 MarketData to its subscribers (ats) and "accepting" OrderData from them submitted by ats OrderRouter
- **ats** is the Algorithmic Trading System itself, whose core module is composed of : 
    - **DataStager** : receives 
        - **MarketData** from the **exchange**, filters them and transforms the interesting ones into **Events** and forwards them to **CEP**, **ODS** and **Monitor UserInterface**
        - **OrderData** from **exchange** related to previously filled orders sent to Exchange by **OrderRouter**, transforms them into **FilledOrders** and forwards them to  **DWH** and **Monitor UserInterface**  
    - **CEP** : receives Events from DataStager, uses **algorithmic strategy** and if the outcome is a processable PartialOrder sends it to OMS
    - **OMS** : receives PartialOrder from CEP, uses a RMS component to check whether the order is "executable" and if so produces a **FilledOrder** and sends it to OrderRouter and DWH
    - **OrderRouter**: receives FilledOrders from OMS, transforms them into OrderData and routes them to the exchange
    - **ODS**: Receives Events from DataStager and tells the EventsGateway to  persist them
    - **DWH**: Receives FilledOrders from OMS and tells the OrderGateway to persist them
    - **Monitor UserInterface**: Receives Events and OrderData from DataStager and pushes them to EventsQueue and OrdersQueue that will be consumed by the websocket handler of an HttpHandler (WebHandler)
    - **WebHandler**: is an http handler with websocket server side support that serves a static file at the following address: [http://localhost:8090](http://localhost:8090), including a websocket to the address [ws://localhost:8090/monitor](ws://localhost:8090/monitor) 
    used to receive realtime Events, FilledOrders and Wallet Balance 


The two main modules **exchange** and **ats** contain an **exchange-main** and an **ats-main** module respectively that produce two standalone, self running **jars**.
 
The above mentioned modules perform all the setUp and wiring of dependencies (plugin), gateways, RMS and all necessary concrete implementations required by the respective core submodules.

ats ad exchange communicate each other through akka remote features(tcp), allowing code to be agnostic about communication internals. 
In the current configuration (exchange/exchange-main/main/resources/application.conf) exchange is available at **localhost:2552** and (ats/ats-main/main/resources/application.conf) ats at **localhost:2553**

##### Why Akka
Despite being reluctant about frameworks usage because they introduce coupling and dependency, I realized that the system I was going to code needed message driven features with all typical problems
related to producer/consumer, asynchronous handling, concurrency, queues, threads with all the risks and possible issues deriving from them.
Furthermore all that complexity started making me loose the focal point of the core  business logic of the system.

Akka, IMHO, is the solution to all the above mentioned problems because takes care of all that complexity and hides it to the developer. 
Last but not least important Akka is a toolkit used in production by companies as Amazon, PayPal, Intel, Norwegian CruiseLine.   


###### Requirements

The following software dependencies are required to run this project:

    - Java JDK 8
    - JUnit 4.12
    - Maven 3

To install the project dependencies, build and install the project artifact and make tests, please run, under the project root folder:

 `mvn clean install`

###### Application Start (CommandLine)

From the project root open two terminal windows and run in each of them, in the following order:

1 - Exchange SubSystem

`java -jar exchange/exchange-main/target/exchange-main.jar`

2 - ATS Subsystem

`java -jar ats/ats-main/target/ats-main.jar`


3 - Once the two jars have started (log will be visible in console), to see RealTime Monitor UI, open the following url on a browser: 
    [http://localhost:8090](http://localhost:8090)
    


###### TODOs
    - Add more Unit Tests and End To End Tests
    - Enforce application robustness through proper exception handling, input validation
    - Replace class costants with resource properties files
    - Tune logging 
    - Implement more realistic event processing strategies (i.e Moving Average, Momentum Trading, Scalping Strategy)
    - Implement more robust Risk Management System 
    - Add Portfolio and Postion Management
    - Implement further InMemory Gateways (i.e Redis) and Persistent ones (MongoDb, MySql)
    - Exploit other Akka features as Akka Cluster, Akka Streams to make the architecture more scalable 

######  Acronyms used
    - ATS : Algorithmic Trading System
    - CEP : Complex Event Processing Engine
    - RMS : Risk Management System
    - OMS : Order Management System
    - DWH : DatawareHouse Store
    - ODS : Operational DataStore

