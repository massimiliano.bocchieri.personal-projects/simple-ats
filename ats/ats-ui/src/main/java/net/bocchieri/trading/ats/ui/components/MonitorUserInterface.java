package net.bocchieri.trading.ats.ui.components;

import akka.actor.AbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import net.bocchieri.trading.ats.core.context.GateKeeper;
import net.bocchieri.trading.ats.core.domain.model.event.Event;
import net.bocchieri.trading.ats.core.domain.model.order.FilledOrder;

import java.util.Date;

public class MonitorUserInterface extends AbstractActor {
  private final LoggingAdapter logger = Logging.getLogger(getContext().system(), this);
  
  @Override
  public void preStart() throws Exception {
    super.preStart();
    
    logger.debug(String.format("MonitorUserInterface started on %s", new Date()));
  }
  
  @Override
  public Receive createReceive() {
    return receiveBuilder()
                   .match(Event.class, this::process)
                   .match(FilledOrder.class, this::process)
                   .build();
  }
  
  private void process(Event event) {
    GateKeeper.monitorUserInterfaceEventsQueue.offer(event);
    logger.debug(String.format("Pushed Incoming Event %s into Events Queue for Monitor UI WebHandler Consumer", event));
  }
  
  private void process(FilledOrder fo) {
    GateKeeper.monitorUserInterfaceFilledOrdersQueue.offer(fo);
    logger.debug(String.format("Pushed Incoming Order %s into Executed Orders Queue for Monitor WebHandler Consumer", fo));
  }
}
