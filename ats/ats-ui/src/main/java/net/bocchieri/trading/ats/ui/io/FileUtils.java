package net.bocchieri.trading.ats.ui.io;

import java.io.*;
import java.util.Objects;

public class FileUtils {
  public static String getFileContent(String filePath) throws IOException {
    String line;
    StringBuilder stringBuilder = new StringBuilder();
    
    try (InputStream is = Objects.requireNonNull(Thread.currentThread().getContextClassLoader().getResourceAsStream(filePath))) {
      BufferedReader reader = new BufferedReader(new InputStreamReader(is));
      while ((line = reader.readLine()) != null)
        stringBuilder.append(line).append(System.lineSeparator());
    }
    
    return stringBuilder.toString();
  }
  
  public static String filePath(String basePath, String fileName) {
    return basePath + File.separator + fileName;
  }
}
