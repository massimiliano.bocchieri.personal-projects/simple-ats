package net.bocchieri.trading.ats.ui.http;

import akka.NotUsed;
import akka.actor.ActorSystem;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.http.javadsl.ConnectHttp;
import akka.http.javadsl.Http;
import akka.http.javadsl.ServerBinding;
import akka.http.javadsl.model.ContentTypes;
import akka.http.javadsl.model.HttpHeader;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import akka.http.javadsl.model.ws.Message;
import akka.http.javadsl.model.ws.TextMessage;
import akka.http.javadsl.model.ws.WebSocket;
import akka.japi.Function;
import akka.japi.JavaPartialFunction;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Source;
import net.bocchieri.trading.ats.core.domain.model.event.Event;
import net.bocchieri.trading.ats.core.domain.model.order.FilledOrder;
import net.bocchieri.trading.ats.core.gateway.WalletGateway;

import java.util.Queue;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static net.bocchieri.trading.ats.ui.io.FileUtils.filePath;
import static net.bocchieri.trading.ats.ui.io.FileUtils.getFileContent;

public class WebHandler {
  private static final String EMPTY_MESSAGE_STRING = "Empty";
  private static final String SERVER_HOST = "localhost";
  private static final int SERVER_PORT = 8090;
  private static final String STATIC_RESOURCES_BASE_PATH = "static";
  private static final int HTTP_NOTFOUND = 404;
  private final ActorSystem system;
  private final Queue<Event> eventsQueue;
  private final Queue<FilledOrder> ordersQueue;
  private final WalletGateway walletGateway;
  private LoggingAdapter logger;
  
  public WebHandler(final ActorSystem system, final Queue<Event> eventsQueue, Queue<FilledOrder> ordersQueue, final WalletGateway walletGateway) {
    this.system = system;
    this.eventsQueue = eventsQueue;
    this.ordersQueue = ordersQueue;
    this.walletGateway = walletGateway;
    
    logger = Logging.getLogger(system, this);
  }
  
  public void execute() throws InterruptedException, ExecutionException, TimeoutException {
    final Materializer materializer = ActorMaterializer.create(system);
    
    final Function<HttpRequest, HttpResponse> handler = this::handleRequest;
    CompletionStage<ServerBinding> serverBindingFuture =
            Http.get(system).bindAndHandleSync(
                    handler, ConnectHttp.toHost(SERVER_HOST, SERVER_PORT), materializer);
    
    serverBindingFuture.toCompletableFuture().get(1, TimeUnit.SECONDS);
  }
  
  private Flow<Message, Message, NotUsed> handler() {
    return
            Flow.<Message>create()
                    .collect(new JavaPartialFunction<Message, Message>() {
                      @Override
                      public Message apply(Message msg, boolean isCheck) {
                        if (isCheck) {
                          if (msg.isText()) {
                            return null;
                          } else {
                            throw noMatch();
                          }
                        } else {
                          return handleTextMessage(msg.asTextMessage());
                        }
                      }
                    });
  }
  
  private HttpResponse handleRequest(HttpRequest request) {
    logger.info(String.format("Handling incoming request %s", request.getUri()));
    
    switch (request.getUri().path()) {
      case "":
      case "/":
        return makeHttpResponse(filePath(STATIC_RESOURCES_BASE_PATH, "index.html"));
      case "/index.js":
        return makeHttpResponse(filePath(STATIC_RESOURCES_BASE_PATH, "index.js"));
      case "/jquery.min.js":
        return makeHttpResponse(filePath(STATIC_RESOURCES_BASE_PATH, "jquery.min.js"));
      case "/monitor":
        final Flow<Message, Message, NotUsed> greeterFlow = handler();
        return WebSocket.handleWebSocketRequestWith(request, greeterFlow);
      default:
        return HttpResponse.create().withStatus(HTTP_NOTFOUND);
    }
  }
  
  private HttpResponse makeHttpResponse(String filePath) {
    try {
      return HttpResponse.create().withEntity(ContentTypes.TEXT_HTML_UTF8, getFileContent(filePath)).addHeader(HttpHeader.parse("Connection", "close"));
    } catch (Throwable t) {
      logger.error(t, "Ops, something went wrong while trying to serve http request, returning HTTP 404");
      return HttpResponse.create().withStatus(HTTP_NOTFOUND);
    }
  }
  
  private TextMessage handleTextMessage(TextMessage msg) {
    switch (msg.getStrictText()) {
      case "credit":
        logger.debug("Websocket Credit request");
        return TextMessage.create(Source.single(String.format("Credit:%,.2f", walletGateway.balance())));
      case "events":
        logger.debug("Websocket Events request");
        return TextMessage.create(Source.single(eventsQueue.isEmpty() ? EMPTY_MESSAGE_STRING : eventsQueue.poll().toString()));
      case "orders":
        logger.debug("Websocket Events request");
        return TextMessage.create(Source.single(ordersQueue.isEmpty() ? EMPTY_MESSAGE_STRING : ordersQueue.poll().toString()));
      default:
        logger.debug("Websocket unhandled request");
        return TextMessage.create(Source.single(EMPTY_MESSAGE_STRING));
    }
  }
}