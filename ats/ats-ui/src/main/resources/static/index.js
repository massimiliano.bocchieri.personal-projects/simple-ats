$(document).ready(function () {
    console.log("Document Loaded!");

    const wsConnectionUri = "ws://localhost:8090/monitor";
    let type = 0;

    if (!("WebSocket" in window)) {
        $('#status').text('WebSocket NOT supported by your Browser!');
    } else {
        const ws = new WebSocket(wsConnectionUri);

        ws.onopen = function () {
            ws.send("Start!");
        };

        ws.onmessage = function (evt) {
            let message = evt.data;

            if (message.indexOf("Credit:") !== -1)
                $('#credit').html(message.replace('Credit:', ''));
            else if (message.indexOf("Event") !== -1)
                $('#events').html(message.replace('Event', '') + "<BR/>" + $('#events').html());
            else if (message.indexOf("Order") !== -1)
                $('#orders').html(message.replace('Order', '') + "<BR/>" + $('#orders').html());

            type = (type + 1) % 3;

            let requestType = "ping";
            switch (type) {
                case 0:
                    requestType = "credit";
                    break;
                case 1:
                    requestType = "events";
                    break;
                case 2:
                    requestType = "orders";
                    break;
            }
            ws.send(requestType);
        };

        ws.onclose = function () {
            $('#status').text('Connection closed!');
        };
    }
});