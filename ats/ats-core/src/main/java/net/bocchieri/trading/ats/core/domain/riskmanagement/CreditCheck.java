package net.bocchieri.trading.ats.core.domain.riskmanagement;

public interface CreditCheck {
  void setCredit(final double amount);
  
  boolean hasEnoughCredit(final double amountToWithdraw);
}
