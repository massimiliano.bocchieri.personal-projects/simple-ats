package net.bocchieri.trading.ats.core.gateway.inmemory;

import net.bocchieri.trading.ats.core.gateway.WalletGateway;

public class InMemoryWalletGateway implements WalletGateway {
  private Double credit;
  
  public InMemoryWalletGateway(double initialAmount) {
    setAmount(initialAmount);
  }
  
  @Override
  public void setAmount(double initialAmount) {
    if (credit != null)
      throw new IllegalArgumentException("Wallet has already been initialized");
    
    this.credit = initialAmount;
  }
  
  @Override
  public void addAmount(double amountToAdd) {
    credit += amountToAdd;
  }
  
  @Override
  public void decreaseAmount(double amountToDecrease) {
    if (amountToDecrease > credit)
      throw new IllegalArgumentException(" Not enough credit");
    
    credit -= amountToDecrease;
  }
  
  @Override
  public double balance() {
    return credit;
  }
}
