package net.bocchieri.trading.ats.core.components;

import akka.actor.AbstractActor;
import akka.actor.ActorSelection;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import net.bocchieri.trading.ats.core.context.GateKeeper;
import net.bocchieri.trading.ats.core.domain.model.order.FilledOrder;
import net.bocchieri.trading.exchange.api.messages.OrderData;

import java.util.Date;

public class OrderRouter extends AbstractActor {
  private final LoggingAdapter logger = Logging.getLogger(getContext().system(), this);
  
  private final ActorSelection exchange;
  
  public OrderRouter(ActorSelection exchange) {
    this.exchange = exchange;
  }
  
  @Override
  public void preStart() throws Exception {
    super.preStart();
    
    logger.info(String.format("OrderRouter started on %s", new Date()));
  }
  
  @Override
  public Receive createReceive() {
    return receiveBuilder()
                   .match(FilledOrder.class, this::process)
                   .build();
  }
  
  @Override
  public void postStop() throws Exception {
    super.postStop();
    
    logger.info(String.format("OrderRouter stopped on %s", new Date()));
  }
  
  private void process(FilledOrder order) {
    routeFilledOrder(transformToOrderData(order));
  }
  
  private void routeFilledOrder(OrderData orderData) {
    logger.info(String.format("Routing OrderData with id <%d> to Exchange %s", orderData.id, exchange.toString()));
    exchange.tell(orderData, self());
  }
  
  private OrderData transformToOrderData(FilledOrder o) {
    return new OrderData(o.id, o.symbol, o.orderSide.toString(), o.price, o.quantity, GateKeeper.ATS_SYSTEM_SUBSCRIBER_ID);
  }
}
