package net.bocchieri.trading.ats.core.gateway;

import net.bocchieri.trading.ats.core.domain.model.order.FilledOrder;

public interface OrderGateway {
  void save(FilledOrder order);
  
  FilledOrder find(long id);
}
