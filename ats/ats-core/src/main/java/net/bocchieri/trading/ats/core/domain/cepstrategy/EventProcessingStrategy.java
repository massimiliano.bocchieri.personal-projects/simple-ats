package net.bocchieri.trading.ats.core.domain.cepstrategy;

import net.bocchieri.trading.ats.core.domain.model.event.Event;
import net.bocchieri.trading.ats.core.domain.model.order.ProposedOrder;

public interface EventProcessingStrategy {
  ProposedOrder generatePartialOrder(Event event);
}
