package net.bocchieri.trading.ats.core.gateway;

import net.bocchieri.trading.ats.core.domain.model.event.Event;

public interface EventGateway {
  void save(Event event);
  
  Event find(long id);
}
