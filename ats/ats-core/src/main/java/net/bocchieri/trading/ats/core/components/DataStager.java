package net.bocchieri.trading.ats.core.components;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import net.bocchieri.trading.ats.core.components.messages.NewEventsSubscription;
import net.bocchieri.trading.ats.core.components.messages.NotifiedOrdersSubscription;
import net.bocchieri.trading.ats.core.domain.filter.MarketDataFilter;
import net.bocchieri.trading.ats.core.domain.model.event.Event;
import net.bocchieri.trading.ats.core.domain.model.order.FilledOrder;
import net.bocchieri.trading.ats.core.domain.model.order.OrderSide;
import net.bocchieri.trading.exchange.api.messages.MarketData;
import net.bocchieri.trading.exchange.api.messages.OrderData;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class DataStager extends AbstractActor {
  private final LoggingAdapter logger = Logging.getLogger(getContext().system(), this);
  
  private final ActorRef operationalDataStore;
  private final ActorRef dataWareHouseStore;
  private final ActorRef complexEventProcessingEngine;
  
  private final List<ActorRef> eventsAdditionalSubscribers;
  private final List<ActorRef> ordersAdditionalSubscribers;
  
  private final MarketDataFilter marketDataFilter;
  
  public DataStager(ActorRef operationalDataStore, ActorRef dataWareHouseStore, ActorRef complexEventProcessingEngine,
                    MarketDataFilter marketDataFilter) {
    this.dataWareHouseStore = dataWareHouseStore;
    this.operationalDataStore = operationalDataStore;
    
    this.complexEventProcessingEngine = complexEventProcessingEngine;
    
    this.marketDataFilter = marketDataFilter;
    
    eventsAdditionalSubscribers = new LinkedList<>();
    ordersAdditionalSubscribers = new LinkedList<>();
  }
  
  @Override
  public void preStart() throws Exception {
    super.preStart();
    
    logger.info(String.format("DataStager started on %s", new Date()));
  }
  
  @Override
  public Receive createReceive() {
    return receiveBuilder()
                   .match(MarketData.class, marketData -> processMarketData(marketData, getSender()))
                   .match(OrderData.class, orderData -> processOrderData(orderData, getSender()))
                   .match(NewEventsSubscription.class, this::subscribeToNewEvents)
                   .match(NotifiedOrdersSubscription.class, this::subscribeToNotifiedOrders)
                   .build();
  }
  
  @Override
  public void postStop() throws Exception {
    super.postStop();
    
    logger.info(String.format("DataStager stopped on %s", new Date()));
  }
  
  private void processMarketData(MarketData marketData, ActorRef exchange) {
    logger.info(String.format("Received MarketData %s from Exchange %s", marketData, exchange.toString()));
    if (marketDataFilter.isInteresting(marketData)) {
      logger.info(String.format("Filter has selected %s", marketData));
      
      Event event = makeEvent(marketData);
      
      complexEventProcessingEngine.tell(event, self());
      
      operationalDataStore.tell(event, self());
      
      notifyEventsToAdditionalSubscribers(event);
    } else {
      logger.info(String.format("Filter has excluded %s", marketData));
    }
  }
  
  private void processOrderData(OrderData orderData, ActorRef exchange) {
    logger.info(String.format("Received OrderData %s Acceptance from Exchange %s", orderData, exchange.toString()));
    
    FilledOrder order = makeOrder(orderData);
    
    notifyAcceptedOrderToDatawareHouseStore(order, dataWareHouseStore);
    
    notifyOrderToAdditionalSubscribers(order);
  }
  
  private void notifyAcceptedOrderToDatawareHouseStore(FilledOrder order, ActorRef dataWareHouseStore) {
    dataWareHouseStore.tell(order, self());
    logger.info(String.format("Notified Order %s to DatawareHouseStore", order));
  }
  
  private void notifyEventsToAdditionalSubscribers(Event e) {
    for (ActorRef additionalSubscriber : eventsAdditionalSubscribers)
      additionalSubscriber.tell(e, self());
  }
  
  private void notifyOrderToAdditionalSubscribers(FilledOrder fo) {
    for (ActorRef additionalSubscriber : ordersAdditionalSubscribers)
      additionalSubscriber.tell(fo, self());
  }
  
  private Event makeEvent(MarketData md) {
    return new Event(md.id, md.symbol, md.bid, md.ask, md.bidSize, md.askSize, md.quoteDateTime);
  }
  
  private FilledOrder makeOrder(OrderData od) {
    return new FilledOrder(od.id, od.symbol, OrderSide.valueOf(od.orderSide.toUpperCase()), od.price, od.quantity);
  }
  
  private void subscribeToNewEvents(NewEventsSubscription subscription) {
    eventsAdditionalSubscribers.add(subscription.subscriber);
  }
  
  private void subscribeToNotifiedOrders(NotifiedOrdersSubscription subscription) {
    ordersAdditionalSubscribers.add(subscription.subscriber);
  }
}
