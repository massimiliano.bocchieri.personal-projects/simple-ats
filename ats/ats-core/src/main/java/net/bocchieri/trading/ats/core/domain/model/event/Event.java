package net.bocchieri.trading.ats.core.domain.model.event;

import java.util.Date;
import java.util.Objects;

public final class Event {
  private static int nextID = 1;
  public final String symbol;
  public final double bid;
  public final double ask;
  public final int bidSize;
  public final int askSize;
  public final Date quoteDateTime;
  public volatile long id;
  public long marketDataId;
  
  public Event(long marketDataId, String symbol, double bid, double ask, int bidSize, int askSize, Date quoteDateTime) {
    this.id = nextID++;
    
    this.marketDataId = marketDataId;
    this.symbol = symbol;
    this.bid = bid;
    this.ask = ask;
    this.bidSize = bidSize;
    this.askSize = askSize;
    this.quoteDateTime = quoteDateTime;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Event event = (Event) o;
    return id == event.id;
  }
  
  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
  
  @Override
  public String toString() {
    return String.format("Event{id=%d, marketDataId=%d, symbol='%s', bid=%s, ask=%s, bidSize=%d, askSize=%d, quoteDateTime=%s}", id, marketDataId, symbol, bid, ask, bidSize, askSize, quoteDateTime);
  }
}
