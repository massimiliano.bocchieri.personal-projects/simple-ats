package net.bocchieri.trading.ats.core.domain.riskmanagement;

import net.bocchieri.trading.ats.core.domain.model.order.FilledOrder;

public interface RiskManager {
  boolean executeOder(FilledOrder filledOrder);
}
