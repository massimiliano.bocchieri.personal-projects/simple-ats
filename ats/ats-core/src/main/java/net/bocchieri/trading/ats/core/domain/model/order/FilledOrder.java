package net.bocchieri.trading.ats.core.domain.model.order;

import java.util.Objects;

public final class FilledOrder {
  public final long id;
  
  public final String symbol;
  public final double price;
  public final int quantity;
  public OrderSide orderSide;
  
  
  public FilledOrder(final long id, final String symbol, final OrderSide orderSide, final double price, final int quantity) {
    this.id = id;
    this.symbol = symbol;
    this.orderSide = orderSide;
    
    this.price = price;
    this.quantity = quantity;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    FilledOrder that = (FilledOrder) o;
    return id == that.id;
  }
  
  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
  
  @Override
  public String toString() {
    return String.format("Order{id=%d, symbol='%s', orderSide=%s, price=%s, quantity=%d}", id, symbol, orderSide, price, quantity);
  }
}
