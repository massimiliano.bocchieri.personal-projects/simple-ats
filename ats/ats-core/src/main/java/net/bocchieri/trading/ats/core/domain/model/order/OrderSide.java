package net.bocchieri.trading.ats.core.domain.model.order;

public enum OrderSide {
  BUY("Buy"), SELL("Sell"), STAY("Stay");
  
  private final String sideName;
  
  OrderSide(final String name) {
    this.sideName = name;
  }
  
  @Override
  public String toString() {
    return sideName;
  }
}
