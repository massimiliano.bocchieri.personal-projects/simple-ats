package net.bocchieri.trading.ats.core.domain.riskmanagement;

import net.bocchieri.trading.ats.core.domain.model.order.FilledOrder;

import static net.bocchieri.trading.ats.core.domain.model.order.OrderSide.SELL;

public class CreditcheckBasedRiskManager implements RiskManager {
  private final CreditCheck creditCheck;
  
  public CreditcheckBasedRiskManager(CreditCheck creditCheck) {
    this.creditCheck = creditCheck;
  }
  
  @Override
  public boolean executeOder(FilledOrder filledOrder) {
    final double totalToTrade = filledOrder.price * (double) filledOrder.quantity;
    
    if (filledOrder.orderSide == SELL) {
      creditCheck.setCredit(totalToTrade);
      return true;
    } else if (creditCheck.hasEnoughCredit(totalToTrade)) {
      creditCheck.setCredit(-totalToTrade);
      return true;
    } else {
      return false;
    }
  }
}
