package net.bocchieri.trading.ats.core.components;

import akka.actor.AbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import net.bocchieri.trading.ats.core.context.GateKeeper;
import net.bocchieri.trading.ats.core.domain.model.order.FilledOrder;

import java.util.Date;

public class DataWareHouseStore extends AbstractActor {
  private final LoggingAdapter logger = Logging.getLogger(getContext().system(), this);
  
  @Override
  public void preStart() throws Exception {
    super.preStart();
    
    logger.debug(String.format("DataWareHouseStore started on %s", new Date()));
  }
  
  @Override
  public Receive createReceive() {
    return receiveBuilder()
                   .match(FilledOrder.class, this::process)
                   .build();
  }
  
  private void process(FilledOrder order) {
    GateKeeper.ordersGateway.save(order);
  }
}
