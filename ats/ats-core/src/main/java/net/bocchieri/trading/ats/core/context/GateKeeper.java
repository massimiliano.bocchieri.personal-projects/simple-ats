package net.bocchieri.trading.ats.core.context;

import net.bocchieri.trading.ats.core.domain.model.event.Event;
import net.bocchieri.trading.ats.core.domain.model.order.FilledOrder;
import net.bocchieri.trading.ats.core.gateway.EventGateway;
import net.bocchieri.trading.ats.core.gateway.OrderGateway;
import net.bocchieri.trading.ats.core.gateway.WalletGateway;

import java.util.Queue;

public class GateKeeper {
  public final static long ATS_SYSTEM_SUBSCRIBER_ID = 1L;
  
  public static EventGateway eventsGateway;
  public static OrderGateway ordersGateway;
  public static WalletGateway walletGateway;
  
  public static Queue<Event> monitorUserInterfaceEventsQueue;
  public static Queue<FilledOrder> monitorUserInterfaceFilledOrdersQueue;
}
