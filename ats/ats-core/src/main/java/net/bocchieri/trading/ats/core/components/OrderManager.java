package net.bocchieri.trading.ats.core.components;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import net.bocchieri.trading.ats.core.domain.model.order.FilledOrder;
import net.bocchieri.trading.ats.core.domain.model.order.ProposedOrder;
import net.bocchieri.trading.ats.core.domain.riskmanagement.RiskManager;

import java.util.Date;

public class OrderManager extends AbstractActor {
  private final LoggingAdapter logger = Logging.getLogger(getContext().system(), this);
  
  private final ActorRef orderRouter;
  private final ActorRef datawareHouseStore;
  
  private final RiskManager riskManager;
  
  public OrderManager(ActorRef orderRouter, ActorRef datawareHouseStore, RiskManager riskManager) {
    this.orderRouter = orderRouter;
    this.datawareHouseStore = datawareHouseStore;
    this.riskManager = riskManager;
  }
  
  @Override
  public void preStart() throws Exception {
    super.preStart();
    
    logger.debug(String.format("OrderManager started on %s", new Date()));
  }
  
  @Override
  public Receive createReceive() {
    return receiveBuilder()
                   .match(ProposedOrder.class, this::process)
                   .build();
  }
  
  private void process(ProposedOrder po) {
    FilledOrder filledOrder = fillProposedOrder(po);
    
    if (riskManager.executeOder(filledOrder)) {
      
      orderRouter.tell(filledOrder, self());
      
      datawareHouseStore.tell(filledOrder, self());
    }
  }
  
  private FilledOrder fillProposedOrder(ProposedOrder po) {
    return new FilledOrder(po.id, po.symbol, po.orderSide, po.price, po.quantity);
  }
}

