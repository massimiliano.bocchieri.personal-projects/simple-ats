package net.bocchieri.trading.ats.core.domain.riskmanagement;

import net.bocchieri.trading.ats.core.context.GateKeeper;

public class SimpleCreditCheck implements CreditCheck {
  @Override
  public void setCredit(double amount) {
    if (amount > 0)
      GateKeeper.walletGateway.addAmount(amount);
    else if (amount < 0)
      GateKeeper.walletGateway.decreaseAmount(Math.abs(amount));
  }
  
  @Override
  public boolean hasEnoughCredit(double amountToWithdraw) {
    return GateKeeper.walletGateway.balance() >= amountToWithdraw;
  }
}
