package net.bocchieri.trading.ats.core.domain.model.order;

import java.util.Objects;

import static net.bocchieri.trading.ats.core.domain.model.order.OrderSide.STAY;

public final class ProposedOrder {
  private static long nextID = 1;
  public final long marketDataId;
  public final String symbol;
  public final double price;
  public final int quantity;
  public volatile long id;
  public OrderSide orderSide;
  
  public ProposedOrder(final long marketDataId, final String symbol, final OrderSide orderSide, final double price, final int quantity) {
    id = nextID++;
    
    this.marketDataId = marketDataId;
    this.symbol = symbol;
    this.orderSide = orderSide;
    
    this.price = price;
    this.quantity = quantity;
  }
  
  public static ProposedOrder makeStayOrder(final long marketDataId, final String symbol) {
    return new ProposedOrder(marketDataId, symbol, STAY, 0, 0);
  }
  
  public boolean isPocessable() {
    return orderSide != STAY;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ProposedOrder that = (ProposedOrder) o;
    return id == that.id;
  }
  
  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
