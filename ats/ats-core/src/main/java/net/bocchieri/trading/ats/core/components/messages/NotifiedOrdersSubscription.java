package net.bocchieri.trading.ats.core.components.messages;

import akka.actor.ActorRef;

public class NotifiedOrdersSubscription {
  public final ActorRef subscriber;
  
  public NotifiedOrdersSubscription(ActorRef subscriber) {
    this.subscriber = subscriber;
  }
}
