package net.bocchieri.trading.ats.core.domain.filter;

import net.bocchieri.trading.exchange.api.messages.MarketData;

import java.util.Set;

public class SymbolBasedMarketDataFilter implements MarketDataFilter {
  private final Set<String> treatedSymbols;
  
  public SymbolBasedMarketDataFilter(Set<String> treatedSymbols) {
    checkValidTreatedSymbols(treatedSymbols);
    this.treatedSymbols = treatedSymbols;
  }
  
  @Override
  public boolean isInteresting(MarketData marketData) {
    return treatedSymbols.contains(marketData.symbol);
  }
  
  private void checkValidTreatedSymbols(Set<String> treatedSymbols) {
    if (null == treatedSymbols)
      throw new IllegalArgumentException("Treated Symbols must be a valid Set");
  }
}
