package net.bocchieri.trading.ats.core.components;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import net.bocchieri.trading.ats.core.domain.cepstrategy.EventProcessingStrategy;
import net.bocchieri.trading.ats.core.domain.model.event.Event;
import net.bocchieri.trading.ats.core.domain.model.order.ProposedOrder;

import java.util.Date;

public class ComplexEventProcessingEngine extends AbstractActor {
  private final LoggingAdapter logger = Logging.getLogger(getContext().system(), this);
  
  private final ActorRef orderManager;
  private final EventProcessingStrategy strategy;
  
  public ComplexEventProcessingEngine(ActorRef orderManager, EventProcessingStrategy strategy) {
    this.orderManager = orderManager;
    this.strategy = strategy;
  }
  
  @Override
  public void preStart() throws Exception {
    super.preStart();
    
    logger.debug(String.format("ComplexEventProcessingEngine started on %s", new Date()));
  }
  
  @Override
  public Receive createReceive() {
    return receiveBuilder()
                   .match(Event.class, this::process)
                   .build();
  }
  
  private void process(Event event) {
    ProposedOrder proposedOrder = strategy.generatePartialOrder(event);
    
    if (proposedOrder.isPocessable())
      orderManager.tell(proposedOrder, self());
    else
      logger.debug(String.format("Event %s generated a STAY Order", event));
  }
}