package net.bocchieri.trading.ats.core.gateway.inmemory;

import net.bocchieri.trading.ats.core.domain.model.event.Event;
import net.bocchieri.trading.ats.core.gateway.EventGateway;

import java.util.HashMap;
import java.util.Map;

public class InMemoryEventGateway implements EventGateway {
  Map<Long, Event> events;
  
  public InMemoryEventGateway() {
    events = new HashMap<>();
  }
  
  @Override
  public void save(Event event) {
    events.put(event.id, event);
  }
  
  @Override
  public Event find(long id) {
    return events.get(id);
  }
  
  @Override
  public String toString() {
    return String.format("InMemoryEventGateway contains %d events", events.size());
  }
}
