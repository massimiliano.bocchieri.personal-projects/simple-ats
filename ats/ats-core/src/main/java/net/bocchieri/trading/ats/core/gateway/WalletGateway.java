package net.bocchieri.trading.ats.core.gateway;

public interface WalletGateway {
  void setAmount(final double initialAmount);
  
  void addAmount(final double amountToAdd);
  
  void decreaseAmount(final double amountToDecrease);
  
  double balance();
}
