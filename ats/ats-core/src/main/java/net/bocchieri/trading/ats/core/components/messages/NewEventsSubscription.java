package net.bocchieri.trading.ats.core.components.messages;

import akka.actor.ActorRef;

public class NewEventsSubscription {
  public final ActorRef subscriber;
  
  public NewEventsSubscription(ActorRef subscriber) {
    this.subscriber = subscriber;
  }
}
