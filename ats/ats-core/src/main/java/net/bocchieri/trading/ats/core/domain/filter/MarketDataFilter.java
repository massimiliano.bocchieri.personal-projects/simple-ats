package net.bocchieri.trading.ats.core.domain.filter;

import net.bocchieri.trading.exchange.api.messages.MarketData;

public interface MarketDataFilter {
  boolean isInteresting(MarketData marketData);
}
