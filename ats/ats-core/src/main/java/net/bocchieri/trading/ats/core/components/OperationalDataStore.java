package net.bocchieri.trading.ats.core.components;

import akka.actor.AbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import net.bocchieri.trading.ats.core.context.GateKeeper;
import net.bocchieri.trading.ats.core.domain.model.event.Event;

import java.util.Date;

public class OperationalDataStore extends AbstractActor {
  private final LoggingAdapter logger = Logging.getLogger(getContext().system(), this);
  
  @Override
  public void preStart() throws Exception {
    super.preStart();
    
    logger.debug(String.format("OperationalDataStore started on %s", new Date()));
  }
  
  @Override
  public Receive createReceive() {
    return receiveBuilder()
                   .match(Event.class, this::process)
                   .build();
  }
  
  private void process(Event event) {
    GateKeeper.eventsGateway.save(event);
  }
}
