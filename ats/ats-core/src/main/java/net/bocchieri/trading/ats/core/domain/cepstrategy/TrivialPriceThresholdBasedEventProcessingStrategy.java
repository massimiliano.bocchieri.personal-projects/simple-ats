package net.bocchieri.trading.ats.core.domain.cepstrategy;

import net.bocchieri.trading.ats.core.domain.model.event.Event;
import net.bocchieri.trading.ats.core.domain.model.order.OrderSide;
import net.bocchieri.trading.ats.core.domain.model.order.ProposedOrder;

import static net.bocchieri.trading.ats.core.domain.model.order.OrderSide.BUY;
import static net.bocchieri.trading.ats.core.domain.model.order.OrderSide.SELL;

public class TrivialPriceThresholdBasedEventProcessingStrategy implements EventProcessingStrategy {
  static final double PRICE_THRESHOLD = 50.00;
  
  @Override
  public ProposedOrder generatePartialOrder(Event event) {
    if (isEventVolumeAffordable(event))
      return decidePartialOrder(event);
    else
      return stayOrder(event);
  }
  
  private boolean isEventVolumeAffordable(Event event) {
    return event.ask <= PRICE_THRESHOLD || event.bid <= PRICE_THRESHOLD;
  }
  
  private ProposedOrder decidePartialOrder(Event event) {
    double price;
    int quantity;
    
    OrderSide orderSide = event.ask < event.bid ? BUY : SELL;
    
    if (orderSide == BUY) {
      price = event.ask;
      quantity = event.askSize;
    } else {
      price = event.bid;
      quantity = event.bidSize;
    }
    
    return new ProposedOrder(event.id, event.symbol, orderSide, price, quantity);
  }
  
  private ProposedOrder stayOrder(Event event) {
    return ProposedOrder.makeStayOrder(event.marketDataId, event.symbol);
  }
}
