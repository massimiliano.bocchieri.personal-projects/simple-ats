package net.bocchieri.trading.ats.core.gateway.inmemory;

import net.bocchieri.trading.ats.core.domain.model.order.FilledOrder;
import net.bocchieri.trading.ats.core.gateway.OrderGateway;

import java.util.HashMap;
import java.util.Map;

public class InMemoryOrderGateway implements OrderGateway {
  Map<Long, FilledOrder> orders;
  
  public InMemoryOrderGateway() {
    orders = new HashMap<>();
  }
  
  @Override
  public void save(FilledOrder order) {
    orders.put(order.id, order);
  }
  
  @Override
  public FilledOrder find(long id) {
    return orders.get(id);
  }
  
  @Override
  public String toString() {
    return String.format("InMemoryOrderGateway contains %d orders", orders.size());
  }
}
