package net.bocchieri.trading.ats.core.domain.cepstrategy;


import net.bocchieri.trading.ats.core.domain.model.event.Event;
import net.bocchieri.trading.ats.core.domain.model.order.ProposedOrder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static net.bocchieri.trading.ats.core.domain.cepstrategy.TrivialPriceThresholdBasedEventProcessingStrategy.PRICE_THRESHOLD;
import static net.bocchieri.trading.ats.core.domain.model.order.OrderSide.*;
import static org.junit.Assert.*;

public class TrivialPriceThresholdBasedEventProcessingStrategyTest {
  private static final Date NOW = new Date();
  private static final int SAMPLE_BID_SIZE = 1;
  private static final int SAMPLE_ASK_SIZE = 12;
  
  private TrivialPriceThresholdBasedEventProcessingStrategy eventProcessingStrategy;
  
  private static Event makeEventOverPriceThreshold() {
    return makeEvent(PRICE_THRESHOLD + 1, PRICE_THRESHOLD + 2, SAMPLE_BID_SIZE, SAMPLE_ASK_SIZE);
  }
  
  private static Event makeEventWithAffordablePrices() {
    return makeEvent(PRICE_THRESHOLD + 1, PRICE_THRESHOLD - 1, SAMPLE_BID_SIZE, SAMPLE_ASK_SIZE);
  }
  
  private static Event makeEvent(double bid, double ask, int bidSize, int askSize) {
    return new Event(1L, "GOOGL", bid, ask, bidSize, askSize, NOW);
  }
  
  @Before
  public void setUp() {
    eventProcessingStrategy = new TrivialPriceThresholdBasedEventProcessingStrategy();
  }
  
  @After
  public void tearDown() {
    eventProcessingStrategy = null;
  }
  
  @Test
  public void givenEventPricesBothOverThreshold_whenGeneratePartialOrderIsInvoked_UnprocessableStayOrderIsGenerated() {
    final ProposedOrder proposedOrder = eventProcessingStrategy.generatePartialOrder(makeEventOverPriceThreshold());
    
    assertFalse(proposedOrder.isPocessable());
    assertEquals(STAY, proposedOrder.orderSide);
  }
  
  @Test
  public void givenEventWithAtLeastOnePriceNotOverThreshold_whenGeneratePartialOrderIsInvoked_ProcessableOrderIsGenerated() {
    final ProposedOrder proposedOrder = eventProcessingStrategy.generatePartialOrder(makeEventWithAffordablePrices());
    
    assertTrue(proposedOrder.isPocessable());
  }
  
  @Test
  public void givenEventWithAskPriceLessThanBidOne_whenGeneratePartialOrderIsInvoked_BuyOrderIsGenerated() {
    final ProposedOrder proposedOrder = eventProcessingStrategy.generatePartialOrder(makeEvent(11.00, 10.00, 1, 2));
    
    assertEquals(BUY, proposedOrder.orderSide);
    assertEquals(10.00, proposedOrder.price, 0);
    assertEquals(2, proposedOrder.quantity);
  }
  
  @Test
  public void givenEventWithBidPriceLessThanAskOne_whenGeneratePartialOrderIsInvoked_SellOrderIsGenerated() {
    final ProposedOrder proposedOrder = eventProcessingStrategy.generatePartialOrder(makeEvent(10.00, 11.00, 1, 2));
    
    assertEquals(SELL, proposedOrder.orderSide);
    assertEquals(10.00, proposedOrder.price, 0);
    assertEquals(1, proposedOrder.quantity);
  }
}