package net.bocchieri.trading.ats.core.components;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.javadsl.TestKit;
import net.bocchieri.trading.ats.core.TestSetup;
import net.bocchieri.trading.ats.core.domain.model.order.FilledOrder;
import net.bocchieri.trading.ats.core.domain.model.order.OrderSide;
import net.bocchieri.trading.ats.core.domain.model.order.ProposedOrder;
import net.bocchieri.trading.ats.core.domain.riskmanagement.RiskManager;
import org.junit.*;

import static net.bocchieri.trading.ats.core.TestSetup.shortWait;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class OrderManagerTest {
  private static final ProposedOrder SAMPLE_PROPOSED_ORDER = new ProposedOrder(1L, "GOOGL", OrderSide.BUY, 100.0, 3);
  private static final FilledOrder EXPECTED_FILLED_ORDER = new FilledOrder(SAMPLE_PROPOSED_ORDER.id, "GOOGL", OrderSide.BUY, 100.0, 3);
  
  private static ActorSystem system;
  private RiskManagerMockedSpy riskManagerMockedSpy;
  
  @BeforeClass
  public static void setUpClass() {
    TestSetup.initContext();
    system = ActorSystem.create();
  }
  
  @AfterClass
  public static void tearDownClass() {
    TestKit.shutdownActorSystem(system);
    system = null;
    
    TestSetup.cleanUpContext();
  }
  
  @Before
  public void setUp() {
    riskManagerMockedSpy = new RiskManagerMockedSpy();
  }
  
  @After
  public void tearDown() {
    riskManagerMockedSpy = null;
  }
  
  @Test
  public void givenFillableOrder_OnReceiveProposedOrder_OrderManagerDispatchesItCorrectly() {
    riskManagerMockedSpy.executeOderReturnValue = true;
    
    final TestKit orderRouterTestProbe = TestSetup.makeTestProbe(system);
    final TestKit datawareHouseStoreTestProbe = TestSetup.makeTestProbe(system);
    final ActorRef orderManager = makeOrderManager(orderRouterTestProbe, datawareHouseStoreTestProbe);
    
    orderManager.tell(SAMPLE_PROPOSED_ORDER, ActorRef.noSender());
    
    assertEquals(EXPECTED_FILLED_ORDER, orderRouterTestProbe.expectMsgClass(shortWait(), FilledOrder.class));
    assertEquals(EXPECTED_FILLED_ORDER, datawareHouseStoreTestProbe.expectMsgClass(shortWait(), FilledOrder.class));
  }
  
  @Test
  public void givenUnfulfillableOrder_OnReceiveProposedOrder_NoReactionIsTriggered() {
    final TestKit orderRouterTestProbe = TestSetup.makeTestProbe(system);
    final TestKit datawareHouseStoreTestProbe = TestSetup.makeTestProbe(system);
    
    riskManagerMockedSpy.executeOderReturnValue = false;
    
    final ActorRef orderManager = makeOrderManager(orderRouterTestProbe, datawareHouseStoreTestProbe);
    
    orderManager.tell(SAMPLE_PROPOSED_ORDER, ActorRef.noSender());
    
    orderRouterTestProbe.expectNoMessage(shortWait());
    datawareHouseStoreTestProbe.expectNoMessage(shortWait());
  }
  
  @Test
  public void testOnReceiveProposedOrderRiskManagerWiring() {
    final TestKit orderRouterTestProbe = TestSetup.makeTestProbe(system);
    final TestKit datawareHouseStoreTestProbe = TestSetup.makeTestProbe(system);
    
    riskManagerMockedSpy.executeOderReturnValue = false;
    
    final ActorRef orderManager = makeOrderManager(orderRouterTestProbe, datawareHouseStoreTestProbe);
    
    orderManager.tell(SAMPLE_PROPOSED_ORDER, ActorRef.noSender());
    
    orderRouterTestProbe.expectNoMessage(shortWait());
    datawareHouseStoreTestProbe.expectNoMessage(shortWait());
    
    assertTrue(riskManagerMockedSpy.executedOrderWasCalled);
    assertEquals(EXPECTED_FILLED_ORDER, riskManagerMockedSpy.inputFilledOrder);
  }
  
  
  @Test
  public void noReactionOnUnhandledMessages() {
    final TestKit orderRouterTestProbe = TestSetup.makeTestProbe(system);
    final TestKit datawareHouseStoreTestProbe = TestSetup.makeTestProbe(system);
    
    final ActorRef orderManager = makeOrderManager(orderRouterTestProbe, datawareHouseStoreTestProbe);
    
    orderManager.tell(new Object(), ActorRef.noSender());
    
    orderRouterTestProbe.expectNoMessage(shortWait());
  }
  
  private ActorRef makeOrderManager(TestKit orderRouterTestProbe, TestKit datawareHouseStoreTestProbe) {
    return system.actorOf(Props.create(OrderManager.class,
            orderRouterTestProbe.getRef(),
            datawareHouseStoreTestProbe.getRef(),
            riskManagerMockedSpy));
  }
  
  private class RiskManagerMockedSpy implements RiskManager {
    boolean executedOrderWasCalled;
    FilledOrder inputFilledOrder;
    
    boolean executeOderReturnValue = true;
    
    @Override
    public boolean executeOder(FilledOrder filledOrder) {
      executedOrderWasCalled = true;
      inputFilledOrder = filledOrder;
      
      return executeOderReturnValue;
    }
  }
}