package net.bocchieri.trading.ats.core.domain.filter;

import net.bocchieri.trading.exchange.api.messages.MarketData;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SymbolBasedMarketDataFilterTest {
  private static final String SAMPLE_ALLOWED_SYMBOL = "GOOGL";
  private static final String SAMPLE_NOT_ALLOWED_SYMBOL = "NAN";
  private static Set<String> ALLOWED_SYMBOLS = new HashSet<>(Arrays.asList("GOOGL", "ION"));
  private SymbolBasedMarketDataFilter marketDataFilter;
  
  @Before
  public void setUp() {
    marketDataFilter = new SymbolBasedMarketDataFilter(ALLOWED_SYMBOLS);
  }
  
  @After
  public void tearDown() {
    marketDataFilter = null;
  }
  
  @Test
  public void givenMarktDataWithAllowedSymbol_whenIsInterestingIsInvoked_TrueIsReturned() {
    assertTrue(marketDataFilter.isInteresting(makeSampleMarketData(SAMPLE_ALLOWED_SYMBOL)));
  }
  
  @Test
  public void givenEventWithNotAllowedSymbol_whenIsInterestingIsInvoked_FalseIsReturned() {
    assertFalse(marketDataFilter.isInteresting(makeSampleMarketData(SAMPLE_NOT_ALLOWED_SYMBOL)));
  }
  
  @Test(expected = IllegalArgumentException.class)
  public void nullTreatedSymbolsSetThrowsException() {
    new SymbolBasedMarketDataFilter(null);
  }
  
  private MarketData makeSampleMarketData(String symbol) {
    return new MarketData(symbol, 10.00, 20.00, 1, 2);
  }
}