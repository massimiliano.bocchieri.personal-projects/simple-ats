package net.bocchieri.trading.ats.core.components;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.javadsl.TestKit;
import net.bocchieri.trading.ats.core.TestSetup;
import net.bocchieri.trading.ats.core.domain.cepstrategy.EventProcessingStrategy;
import net.bocchieri.trading.ats.core.domain.model.event.Event;
import net.bocchieri.trading.ats.core.domain.model.order.OrderSide;
import net.bocchieri.trading.ats.core.domain.model.order.ProposedOrder;
import org.junit.*;

import java.util.Date;

import static net.bocchieri.trading.ats.core.TestSetup.makeTestProbe;
import static net.bocchieri.trading.ats.core.TestSetup.shortWait;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ComplexEventProcessingEngineTest {
  private static final Date NOW = new Date();
  private static final Event SAMPLE_EVENT = new Event(1L, "GOOGL", 10.0, 20.0, 2, 3, NOW);
  private static final ProposedOrder EXPECTED_PROPOSED_ORDER = new ProposedOrder(1L, "GOOGL", OrderSide.BUY, 100.0, 3);
  
  private static ActorSystem system;
  private EventProcessingStrategyMockedSpy eventProcessingStrategyMockedSpy;
  
  @BeforeClass
  public static void setUpClass() {
    TestSetup.initContext();
    system = ActorSystem.create();
  }
  
  @AfterClass
  public static void tearDownClass() {
    TestKit.shutdownActorSystem(system);
    system = null;
    
    TestSetup.cleanUpContext();
  }
  
  @Before
  public void setUp() {
    eventProcessingStrategyMockedSpy = new EventProcessingStrategyMockedSpy();
  }
  
  @After
  public void tearDown() {
    eventProcessingStrategyMockedSpy = null;
  }
  
  @Test
  public void givenEventTriggeringProcessableProposedOrder_OnReceiveEvent_ComplexEventProcessingEngineDispatchesItCorrectly() {
    eventProcessingStrategyMockedSpy.proposedOrderToReturn = EXPECTED_PROPOSED_ORDER;
    
    final TestKit orderManagerTestProbe = makeTestProbe(system);
    final ActorRef complexEventProcessingEngine = makeComplexEventProcessingEngine(orderManagerTestProbe);
    
    complexEventProcessingEngine.tell(SAMPLE_EVENT, ActorRef.noSender());
    
    ProposedOrder responseMessageForOrderManager = orderManagerTestProbe.expectMsgClass(shortWait(), ProposedOrder.class);
    assertEquals(EXPECTED_PROPOSED_ORDER, responseMessageForOrderManager);
  }
  
  @Test
  public void givenEventTriggeringNoProposedOrder_OnReceiveEvent_NoActionIsTriggered() {
    eventProcessingStrategyMockedSpy.proposedOrderToReturn = ProposedOrder.makeStayOrder(1L, "GOOGL");
    
    final TestKit orderManagerTestProbe = makeTestProbe(system);
    final ActorRef complexEventProcessingEngine = makeComplexEventProcessingEngine(orderManagerTestProbe);
    
    complexEventProcessingEngine.tell(SAMPLE_EVENT, ActorRef.noSender());
    
    orderManagerTestProbe.expectNoMessage(shortWait());
  }
  
  @Test
  public void testOnReceiveProposedOrderRiskManagerWiring() {
    eventProcessingStrategyMockedSpy.proposedOrderToReturn = EXPECTED_PROPOSED_ORDER;
    
    final TestKit orderManagerTestProbe = makeTestProbe(system);
    final ActorRef complexEventProcessingEngine = makeComplexEventProcessingEngine(orderManagerTestProbe);
    
    complexEventProcessingEngine.tell(SAMPLE_EVENT, ActorRef.noSender());
    
    orderManagerTestProbe.expectMsgClass(shortWait(), ProposedOrder.class);
    
    assertTrue(eventProcessingStrategyMockedSpy.generatePartialOrderWasCalled);
    assertEquals(SAMPLE_EVENT, eventProcessingStrategyMockedSpy.inputEvent);
  }
  
  
  @Test
  public void noReactionOnUnhandledMessages() {
    final TestKit orderManagerTestProbe = makeTestProbe(system);
    final ActorRef complexEventProcessingEngine = makeComplexEventProcessingEngine(orderManagerTestProbe);
    
    complexEventProcessingEngine.tell(new Object(), ActorRef.noSender());
    
    orderManagerTestProbe.expectNoMessage(shortWait());
  }
  
  private ActorRef makeComplexEventProcessingEngine(TestKit orderManagerTestProbe) {
    return system.actorOf(Props.create(ComplexEventProcessingEngine.class,
            orderManagerTestProbe.getRef(),
            eventProcessingStrategyMockedSpy));
  }
  
  private class EventProcessingStrategyMockedSpy implements EventProcessingStrategy {
    boolean generatePartialOrderWasCalled;
    Event inputEvent;
    
    ProposedOrder proposedOrderToReturn;
    
    @Override
    public ProposedOrder generatePartialOrder(Event event) {
      inputEvent = event;
      generatePartialOrderWasCalled = true;
      return proposedOrderToReturn;
    }
  }
  
  
}