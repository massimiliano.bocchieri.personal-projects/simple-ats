package net.bocchieri.trading.ats.core.components;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.javadsl.TestKit;
import net.bocchieri.trading.ats.core.TestSetup;
import net.bocchieri.trading.ats.core.context.GateKeeper;
import net.bocchieri.trading.ats.core.domain.filter.MarketDataFilter;
import net.bocchieri.trading.ats.core.domain.model.event.Event;
import net.bocchieri.trading.ats.core.domain.model.order.FilledOrder;
import net.bocchieri.trading.exchange.api.messages.MarketData;
import net.bocchieri.trading.exchange.api.messages.OrderData;
import org.junit.*;

import static net.bocchieri.trading.ats.core.TestSetup.makeTestProbe;
import static net.bocchieri.trading.ats.core.TestSetup.shortWait;

public class DataStagerTest {
  private static final MarketData SAMPLE_MARKET_DATA = new MarketData("GOOGL", 50.00, 60.00, 1, 2);
  private static final OrderData SAMPLE_ORDER_DATA = new OrderData(1L, "GOOGL", "Buy", 100.00, 3, GateKeeper.ATS_SYSTEM_SUBSCRIBER_ID);
  
  private static ActorSystem system;
  private MarketDataFilterMockedSpy marketDataFilterMockedSpy;
  
  @BeforeClass
  public static void setUpClass() {
    TestSetup.initContext();
    system = ActorSystem.create();
  }
  
  @AfterClass
  public static void tearDownClass() {
    TestKit.shutdownActorSystem(system);
    system = null;
    
    TestSetup.cleanUpContext();
  }
  
  @Before
  public void setUp() {
    marketDataFilterMockedSpy = new MarketDataFilterMockedSpy();
  }
  
  @After
  public void tearDown() {
    marketDataFilterMockedSpy = null;
  }
  
  @Test
  public void givenInterestingMarketData_OnReceiveMarketData_DataStagerTransformsItToEventAndDispatchesCorrectly() {
    final TestKit operationalDataStoreTestProbe = makeTestProbe(system);
    final TestKit dataWareHouseStoreTestProbe = makeTestProbe(system);
    final TestKit complexEventProcessingEngineTestProbe = makeTestProbe(system);
    
    marketDataFilterMockedSpy.returnValue = true;
    
    final ActorRef dataStager = makeDataStager(dataWareHouseStoreTestProbe, operationalDataStoreTestProbe, complexEventProcessingEngineTestProbe);
    
    dataStager.tell(SAMPLE_MARKET_DATA, ActorRef.noSender());
    
    operationalDataStoreTestProbe.expectMsgClass(shortWait(), Event.class);
    dataWareHouseStoreTestProbe.expectNoMessage(shortWait());
    complexEventProcessingEngineTestProbe.expectMsgClass(shortWait(), Event.class);
  }
  
  @Test
  public void givenInterestingMarketData_OnReceiveMarketData_noReactionIsTriggered() {
    final TestKit operationalDataStoreTestProbe = makeTestProbe(system);
    final TestKit dataWareHouseStoreTestProbe = makeTestProbe(system);
    final TestKit complexEventProcessingEngineTestProbe = makeTestProbe(system);
    
    marketDataFilterMockedSpy.returnValue = false;
    
    final ActorRef dataStager = makeDataStager(dataWareHouseStoreTestProbe, operationalDataStoreTestProbe, complexEventProcessingEngineTestProbe);
    
    dataStager.tell(new Object(), ActorRef.noSender());
    
    operationalDataStoreTestProbe.expectNoMessage(shortWait());
    dataWareHouseStoreTestProbe.expectNoMessage(shortWait());
    complexEventProcessingEngineTestProbe.expectNoMessage(shortWait());
  }
  
  @Test
  public void onReceiveOrderData_DataStagerTransformsItToFilledOrderAndDispatchesCorrectly() {
    final TestKit operationalDataStoreTestProbe = makeTestProbe(system);
    final TestKit dataWareHouseStoreTestProbe = makeTestProbe(system);
    final TestKit complexEventProcessingEngineTestProbe = makeTestProbe(system);
    
    final ActorRef dataStager = makeDataStager(dataWareHouseStoreTestProbe, operationalDataStoreTestProbe, complexEventProcessingEngineTestProbe);
    
    dataStager.tell(SAMPLE_ORDER_DATA, ActorRef.noSender());
    
    operationalDataStoreTestProbe.expectNoMessage(shortWait());
    dataWareHouseStoreTestProbe.expectMsgClass(shortWait(), FilledOrder.class);
    complexEventProcessingEngineTestProbe.expectNoMessage(shortWait());
  }
  
  @Test
  public void noReactionOnUnhandledMessages() {
    final TestKit operationalDataStoreTestProbe = makeTestProbe(system);
    final TestKit dataWareHouseStoreTestProbe = makeTestProbe(system);
    final TestKit complexEventProcessingEngineTestProbe = makeTestProbe(system);
    
    final ActorRef dataStager = makeDataStager(dataWareHouseStoreTestProbe, operationalDataStoreTestProbe, complexEventProcessingEngineTestProbe);
    
    dataStager.tell(new Object(), ActorRef.noSender());
    
    operationalDataStoreTestProbe.expectNoMessage(shortWait());
    dataWareHouseStoreTestProbe.expectNoMessage(shortWait());
    complexEventProcessingEngineTestProbe.expectNoMessage(shortWait());
  }
  
  private ActorRef makeDataStager(final TestKit dataWareHouseStoreTestProbe, final TestKit operationalDataStoreTestProbe, final TestKit complexEventProcessingEngineTestProbe) {
    return system.actorOf(Props.create(DataStager.class,
            operationalDataStoreTestProbe.getRef(),
            dataWareHouseStoreTestProbe.getRef(),
            complexEventProcessingEngineTestProbe.getRef(),
            marketDataFilterMockedSpy));
  }
  
  
  private class MarketDataFilterMockedSpy implements MarketDataFilter {
    boolean isInterestingWasCalled;
    MarketData inputMarketData;
    boolean returnValue;
    
    @Override
    public boolean isInteresting(MarketData marketData) {
      isInterestingWasCalled = true;
      inputMarketData = marketData;
      
      return returnValue;
    }
  }
}