package net.bocchieri.trading.ats.core;

import akka.actor.ActorSystem;
import akka.testkit.javadsl.TestKit;
import net.bocchieri.trading.ats.core.context.GateKeeper;
import net.bocchieri.trading.ats.core.gateway.inmemory.InMemoryEventGateway;
import net.bocchieri.trading.ats.core.gateway.inmemory.InMemoryOrderGateway;
import net.bocchieri.trading.ats.core.gateway.inmemory.InMemoryWalletGateway;

import java.time.Duration;
import java.util.LinkedList;

public class TestSetup {
  public static final double INITIAL_WALLET_CREDIT = 1000000.00;
  
  public static void initContext() {
    GateKeeper.eventsGateway = new InMemoryEventGateway();
    GateKeeper.ordersGateway = new InMemoryOrderGateway();
    GateKeeper.walletGateway = new InMemoryWalletGateway(INITIAL_WALLET_CREDIT);
    
    GateKeeper.monitorUserInterfaceEventsQueue = new LinkedList<>();
    GateKeeper.monitorUserInterfaceFilledOrdersQueue = new LinkedList<>();
  }
  
  public static void cleanUpContext() {
    GateKeeper.eventsGateway = null;
    GateKeeper.ordersGateway = null;
    GateKeeper.walletGateway = null;
    
    GateKeeper.monitorUserInterfaceEventsQueue = null;
    GateKeeper.monitorUserInterfaceFilledOrdersQueue = null;
  }
  
  public static TestKit makeTestProbe(ActorSystem system) {
    return new TestKit(system);
  }
  
  public static Duration shortWait() {
    return Duration.ofMillis(20);
  }
  
  public static Duration longWait() {
    return Duration.ofMillis(50);
  }
}
