package net.bocchieri.trading.ats.core.components;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.javadsl.TestKit;
import net.bocchieri.trading.ats.core.TestSetup;
import net.bocchieri.trading.ats.core.context.GateKeeper;
import net.bocchieri.trading.ats.core.domain.model.order.FilledOrder;
import net.bocchieri.trading.ats.core.domain.model.order.OrderSide;
import org.junit.*;

import java.util.Date;

import static net.bocchieri.trading.ats.core.TestSetup.makeTestProbe;
import static net.bocchieri.trading.ats.core.TestSetup.shortWait;
import static org.junit.Assert.assertEquals;

public class DataWareHouseStoreTest {
  private static final Date NOW = new Date();
  private static final FilledOrder SAMPLE_FILLED_ORDER = new FilledOrder(1L, "GOOGL", OrderSide.BUY, 10.00, 3);
  
  private static ActorSystem system;
  
  @BeforeClass
  public static void setUpClass() {
    system = ActorSystem.create();
  }
  
  @AfterClass
  public static void tearDownClass() {
    TestKit.shutdownActorSystem(system);
    system = null;
  }
  
  @Before
  public void setUp() {
    TestSetup.initContext();
  }
  
  @After
  public void tearDown() {
    TestSetup.cleanUpContext();
  }
  
  @Test
  public void onReceiveFilledOrder_DataWareHouseStorePersistsIt() {
    final TestKit testProbe = makeTestProbe(system);
    final ActorRef datawareHouseStore = makeDatawareHouseStore();
    
    datawareHouseStore.tell(SAMPLE_FILLED_ORDER, ActorRef.noSender());
    
    testProbe.expectNoMessage(shortWait());
    assertEquals(SAMPLE_FILLED_ORDER, GateKeeper.ordersGateway.find(SAMPLE_FILLED_ORDER.id));
  }
  
  @Test
  public void noReactionOnUnhandledMessages() {
    final TestKit testProbe = makeTestProbe(system);
    final ActorRef datawareHouseStore = makeDatawareHouseStore();
    
    datawareHouseStore.tell(new Object(), ActorRef.noSender());
    
    testProbe.expectNoMessage(shortWait());
  }
  
  private ActorRef makeDatawareHouseStore() {
    return system.actorOf(Props.create(DataWareHouseStore.class));
  }
}