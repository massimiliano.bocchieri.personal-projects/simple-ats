package net.bocchieri.trading.ats.core.domain.riskmanagement;

import net.bocchieri.trading.ats.core.gateway.inmemory.InMemoryWalletGateway;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static net.bocchieri.trading.ats.core.context.GateKeeper.walletGateway;
import static org.junit.Assert.*;

public class SimpleCreditCheckTest {
  private static final double INITIAL_CREDIT = 10.00;
  
  private CreditCheck creditCheck;
  
  @Before
  public void setUp() {
    creditCheck = new SimpleCreditCheck();
  }
  
  @After
  public void tearDown() {
    creditCheck = null;
  }
  
  @Test
  public void givenPositiveAmount_whenSetCreditIsInvoked_ThatAmountIsAddedToWalletBalance() {
    walletGateway = new InMemoryWalletGateway(INITIAL_CREDIT);
    
    creditCheck.setCredit(10.00);
    
    assertEquals(INITIAL_CREDIT + 10.00, walletGateway.balance(), 0);
  }
  
  @Test
  public void givenNegativeAmount_whenSetCreditIsInvoked_ThatAmountIsSubtractedFromWalletBalance() {
    walletGateway = new InMemoryWalletGateway(20.00);
    
    creditCheck.setCredit(-10.00);
    
    assertEquals(20.00 - 10.00, walletGateway.balance(), 0);
  }
  
  @Test
  public void givenZeroAmount_whenSetCreditIsInvoked_WalletBalanceStaysTheSame() {
    walletGateway = new InMemoryWalletGateway(20.00);
    
    creditCheck.setCredit(0);
    
    assertEquals(20.00, walletGateway.balance(), 0);
  }
  
  @Test
  public void givenAmountToWithdrawGreaterThanBalance_whenHasEnoughCreditIsInvoked_FalseIsReturned() {
    walletGateway = new InMemoryWalletGateway(INITIAL_CREDIT);
    assertFalse(creditCheck.hasEnoughCredit(INITIAL_CREDIT + 1));
  }
  
  @Test
  public void givenAmountToWithdrawLessThanOrEqualToBalance_whenHasEnoughCreditIsInvoked_TrueIsReturned() {
    walletGateway = new InMemoryWalletGateway(INITIAL_CREDIT);
    
    assertTrue(creditCheck.hasEnoughCredit(INITIAL_CREDIT - 1.00));
    assertTrue(creditCheck.hasEnoughCredit(INITIAL_CREDIT));
  }
}