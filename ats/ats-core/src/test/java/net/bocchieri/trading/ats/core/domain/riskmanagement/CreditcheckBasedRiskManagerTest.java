package net.bocchieri.trading.ats.core.domain.riskmanagement;

import net.bocchieri.trading.ats.core.domain.model.order.FilledOrder;
import net.bocchieri.trading.ats.core.domain.model.order.OrderSide;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static net.bocchieri.trading.ats.core.domain.model.order.OrderSide.BUY;
import static net.bocchieri.trading.ats.core.domain.model.order.OrderSide.SELL;
import static org.junit.Assert.*;

public class CreditcheckBasedRiskManagerTest {
  private static final double SAMPLE_PRICE = 10.00;
  private static final int SAMPLE_QUANTITY = 2;
  
  private CreditcheckBasedRiskManager riskManager;
  private CreditCheckMockedSpy creditCheckMockedSpy;
  
  
  @Before
  public void setUp() {
    creditCheckMockedSpy = new CreditCheckMockedSpy();
    riskManager = new CreditcheckBasedRiskManager(creditCheckMockedSpy);
  }
  
  @After
  public void tearDown() {
    creditCheckMockedSpy = null;
    riskManager = null;
  }
  
  @Test
  public void givenSellFilledOrder_whenExecuteOrderIsInvoked_OrderIsExecutedAccordnglyAndTrueIsReturned() {
    final boolean isOrderExecuted = riskManager.executeOder(makeSampleFilledOrder(SELL));
    
    assertFalse(creditCheckMockedSpy.hasEnoughCreditWasCalled);
    assertTrue(creditCheckMockedSpy.setCreditWasCalled);
    assertEquals(SAMPLE_PRICE * SAMPLE_QUANTITY, creditCheckMockedSpy.amount, 0);
    assertTrue(isOrderExecuted);
  }
  
  
  @Test
  public void givenBuyFilledOrderWithEnoughCredit_whenExecuteOrderIsInvoked_OrderIsExecutedAccordnglyAndTrueIsReturned() {
    creditCheckMockedSpy.hasEnoughCreditReturnValue = true;
    
    final boolean isOrderExecuted = riskManager.executeOder(makeSampleFilledOrder(BUY));
    
    assertTrue(creditCheckMockedSpy.hasEnoughCreditWasCalled);
    assertEquals(SAMPLE_PRICE * SAMPLE_QUANTITY, creditCheckMockedSpy.amountToWithdraw, 0);
    assertTrue(creditCheckMockedSpy.setCreditWasCalled);
    assertEquals(-(SAMPLE_PRICE * SAMPLE_QUANTITY), creditCheckMockedSpy.amount, 0);
    assertTrue(isOrderExecuted);
  }
  
  @Test
  public void givenBuyFilledOrderWithoutEnoughCredit_whenExecuteOrderIsInvoked_OrderIsNotExecutedAndTrueIsReturned() {
    creditCheckMockedSpy.hasEnoughCreditReturnValue = false;
    
    final boolean isOrderExecuted = riskManager.executeOder(makeSampleFilledOrder(BUY));
    
    assertTrue(creditCheckMockedSpy.hasEnoughCreditWasCalled);
    assertEquals(SAMPLE_PRICE * SAMPLE_QUANTITY, creditCheckMockedSpy.amountToWithdraw, 0);
    assertFalse(creditCheckMockedSpy.setCreditWasCalled);
    assertFalse(isOrderExecuted);
  }
  
  private FilledOrder makeSampleFilledOrder(OrderSide orderSide) {
    return new FilledOrder(1L, "GOOGL", orderSide, SAMPLE_PRICE, SAMPLE_QUANTITY);
  }
  
  private class CreditCheckMockedSpy implements CreditCheck {
    private double amountToWithdraw;
    private boolean hasEnoughCreditWasCalled;
    private double amount;
    private boolean setCreditWasCalled;
    private boolean hasEnoughCreditReturnValue;
    
    @Override
    public void setCredit(double amount) {
      setCreditWasCalled = true;
      this.amount = amount;
    }
    
    @Override
    public boolean hasEnoughCredit(double amountToWithdraw) {
      hasEnoughCreditWasCalled = true;
      this.amountToWithdraw = amountToWithdraw;
      
      return hasEnoughCreditReturnValue;
    }
  }
}