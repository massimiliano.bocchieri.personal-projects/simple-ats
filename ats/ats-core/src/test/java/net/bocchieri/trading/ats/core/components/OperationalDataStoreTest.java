package net.bocchieri.trading.ats.core.components;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.javadsl.TestKit;
import net.bocchieri.trading.ats.core.TestSetup;
import net.bocchieri.trading.ats.core.context.GateKeeper;
import net.bocchieri.trading.ats.core.domain.model.event.Event;
import org.junit.*;

import java.util.Date;

import static net.bocchieri.trading.ats.core.TestSetup.makeTestProbe;
import static net.bocchieri.trading.ats.core.TestSetup.shortWait;
import static org.junit.Assert.assertEquals;

public class OperationalDataStoreTest {
  private static final Date NOW = new Date();
  private static final Event SAMPLE_EVENT = new Event(1L, "GOOGL", 10.00, 20.00, 2, 3, NOW);
  
  private static ActorSystem system;
  
  @BeforeClass
  public static void setUpClass() {
    system = ActorSystem.create();
  }
  
  @AfterClass
  public static void tearDownClass() {
    TestKit.shutdownActorSystem(system);
    system = null;
  }
  
  @Before
  public void setUp() {
    TestSetup.initContext();
  }
  
  @After
  public void tearDown() {
    TestSetup.cleanUpContext();
  }
  
  @Test
  public void onReceiveEvent_OperationalDataStorePersistsIt() {
    final TestKit testProbe = makeTestProbe(system);
    final ActorRef operationalDataStore = makeOperationalDataStore();
    
    operationalDataStore.tell(SAMPLE_EVENT, ActorRef.noSender());
    
    testProbe.expectNoMessage(shortWait());
    assertEquals(SAMPLE_EVENT, GateKeeper.eventsGateway.find(SAMPLE_EVENT.id));
  }
  
  @Test
  public void noReactionOnUnhandledMessages() {
    final TestKit testProbe = makeTestProbe(system);
    final ActorRef operationalDataStore = makeOperationalDataStore();
    
    operationalDataStore.tell(new Object(), ActorRef.noSender());
    
    testProbe.expectNoMessage(shortWait());
  }
  
  private ActorRef makeOperationalDataStore() {
    return system.actorOf(Props.create(OperationalDataStore.class));
  }
}