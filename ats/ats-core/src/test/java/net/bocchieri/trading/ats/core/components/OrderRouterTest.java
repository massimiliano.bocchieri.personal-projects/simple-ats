package net.bocchieri.trading.ats.core.components;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.javadsl.TestKit;
import net.bocchieri.trading.ats.core.TestSetup;
import net.bocchieri.trading.ats.core.context.GateKeeper;
import net.bocchieri.trading.ats.core.domain.model.order.FilledOrder;
import net.bocchieri.trading.ats.core.domain.model.order.OrderSide;
import net.bocchieri.trading.exchange.api.messages.OrderData;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static net.bocchieri.trading.ats.core.TestSetup.*;
import static org.junit.Assert.assertEquals;

public class OrderRouterTest {
  private static final FilledOrder SAMPLE_FILLED_ORDER = new FilledOrder(1L, "GOOGL", OrderSide.BUY, 100.0, 3);
  private static ActorSystem system;
  
  @BeforeClass
  public static void setUpClass() {
    TestSetup.initContext();
    system = ActorSystem.create();
  }
  
  @AfterClass
  public static void tearDownClass() {
    TestKit.shutdownActorSystem(system);
    system = null;
    
    TestSetup.cleanUpContext();
  }
  
  private ActorSelection actorRefToSelection(TestKit exchangeProbe) {
    return system.actorSelection(exchangeProbe.getRef().path());
  }
  
  @Test
  public void onReceiveOrder_OrderRouterRoutesOrderDataToExchange() {
    final TestKit exchangeTestProbe = makeTestProbe(system);
    final ActorRef orderRouter = makeOrderRouter(exchangeTestProbe);
    
    orderRouter.tell(SAMPLE_FILLED_ORDER, exchangeTestProbe.getRef());
    
    OrderData response = exchangeTestProbe.expectMsgClass(longWait(), OrderData.class);
    assertEquals(SAMPLE_FILLED_ORDER.id, response.id);
    assertEquals(SAMPLE_FILLED_ORDER.symbol, response.symbol);
    assertEquals(SAMPLE_FILLED_ORDER.orderSide.toString(), response.orderSide);
    assertEquals(SAMPLE_FILLED_ORDER.price, response.price, 0);
    assertEquals(SAMPLE_FILLED_ORDER.quantity, response.quantity);
    assertEquals(GateKeeper.ATS_SYSTEM_SUBSCRIBER_ID, response.subscriberId);
  }
  
  @Test
  public void noReactionOnUnhandledMessages() {
    final TestKit exchangeTestProbe = makeTestProbe(system);
    final ActorRef orderRouter = makeOrderRouter(exchangeTestProbe);
    
    orderRouter.tell(new Object(), ActorRef.noSender());
    
    exchangeTestProbe.expectNoMessage(shortWait());
  }
  
  private ActorRef makeOrderRouter(TestKit exchangeTestProbe) {
    return system.actorOf(Props.create(OrderRouter.class, actorRefToSelection(exchangeTestProbe)));
  }
}