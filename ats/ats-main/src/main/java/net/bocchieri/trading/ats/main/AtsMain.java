package net.bocchieri.trading.ats.main;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.typesafe.config.ConfigFactory;
import net.bocchieri.trading.ats.core.components.*;
import net.bocchieri.trading.ats.core.components.messages.NewEventsSubscription;
import net.bocchieri.trading.ats.core.components.messages.NotifiedOrdersSubscription;
import net.bocchieri.trading.ats.core.context.GateKeeper;
import net.bocchieri.trading.ats.core.domain.cepstrategy.EventProcessingStrategy;
import net.bocchieri.trading.ats.core.domain.cepstrategy.TrivialPriceThresholdBasedEventProcessingStrategy;
import net.bocchieri.trading.ats.core.domain.filter.MarketDataFilter;
import net.bocchieri.trading.ats.core.domain.filter.SymbolBasedMarketDataFilter;
import net.bocchieri.trading.ats.core.domain.riskmanagement.CreditcheckBasedRiskManager;
import net.bocchieri.trading.ats.core.domain.riskmanagement.RiskManager;
import net.bocchieri.trading.ats.core.domain.riskmanagement.SimpleCreditCheck;
import net.bocchieri.trading.ats.core.gateway.inmemory.InMemoryEventGateway;
import net.bocchieri.trading.ats.core.gateway.inmemory.InMemoryOrderGateway;
import net.bocchieri.trading.ats.core.gateway.inmemory.InMemoryWalletGateway;
import net.bocchieri.trading.ats.ui.components.MonitorUserInterface;
import net.bocchieri.trading.ats.ui.http.WebHandler;
import net.bocchieri.trading.exchange.api.messages.Subscription;
import net.bocchieri.trading.exchange.api.messages.UnSubscription;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class AtsMain {
  private static final String ATS_SYSTEM_ID = "SimpleAtsSystem";
  
  private static final String REMOTE_EXCHANGE_SYSTEM_ID = "RemoteExchangeSystem";
  private static final String REMOTE_EXCHANGE_SYSTEM_HOST = "127.0.0.1";
  private static final String REMOTE_EXCHANGE_SYSTEM_PORT = "2552";
  private static final String REMOTE_EXCHANGE_ID = "Exchange";
  private static final String REMOTE_EXCHANGE_PATH = String.format("akka.tcp://%s@%s:%s/user/%s",
          REMOTE_EXCHANGE_SYSTEM_ID, REMOTE_EXCHANGE_SYSTEM_HOST, REMOTE_EXCHANGE_SYSTEM_PORT, REMOTE_EXCHANGE_ID);
  
  private static final int INITIAL_WALLET_CREDIT = 100000;
  
  private ActorSystem system;
  
  private ActorSelection exchange;
  
  private ActorRef ods;
  private ActorRef dwhs;
  private ActorRef orderRouter;
  private ActorRef orderManager;
  private ActorRef cep;
  private ActorRef dataStager;
  private ActorRef monitorUI;
  
  public static void main(String[] args) {
    AtsMain main = new AtsMain();
    
    main.setUpGateways();
    
    main.initSystem();
    main.setUpRemoteExchange();
    main.setUpActors();
    main.subscribeMonitorUIToEventsAndNotifedOrdersInDataStager();
    main.subscribeDataStagerToExchange();
    
    main.initMonitorUSerInterfaceQueues();
    main.invokeMonitorUserInterfaceWebHandler();
    
    main.handleExecutionFlow();
  }
  
  private void initMonitorUSerInterfaceQueues() {
    GateKeeper.monitorUserInterfaceEventsQueue = new LinkedList<>();
    GateKeeper.monitorUserInterfaceFilledOrdersQueue = new LinkedList<>();
  }
  
  private void invokeMonitorUserInterfaceWebHandler() {
    try {
      new WebHandler(system, GateKeeper.monitorUserInterfaceEventsQueue, GateKeeper.monitorUserInterfaceFilledOrdersQueue, GateKeeper.walletGateway).execute();
    } catch (InterruptedException | ExecutionException | TimeoutException e) {
      System.err.println("Http Component closed unexpectedly...");
      e.printStackTrace();
    }
  }
  
  private void setUpGateways() {
    GateKeeper.eventsGateway = new InMemoryEventGateway();
    GateKeeper.ordersGateway = new InMemoryOrderGateway();
    GateKeeper.walletGateway = new InMemoryWalletGateway(INITIAL_WALLET_CREDIT);
  }
  
  private void setUpRemoteExchange() {
    exchange = system.actorSelection(REMOTE_EXCHANGE_PATH);
  }
  
  private void initSystem() {
    system = ActorSystem.create(ATS_SYSTEM_ID, ConfigFactory.load());
  }
  
  private void setUpActors() {
    ods = system.actorOf(Props.create(OperationalDataStore.class), "OperationalDataStore");
    dwhs = system.actorOf(Props.create(DataWareHouseStore.class), "DataWareHouseStore");
    orderRouter = system.actorOf(Props.create(OrderRouter.class, exchange), "OrderRouter");
    
    orderManager = system.actorOf(Props.create(OrderManager.class, orderRouter, dwhs, chooseRiskManager()), "OrderManager");
    
    cep = system.actorOf(Props.create(ComplexEventProcessingEngine.class, orderManager, chooseEventProcessingStrategy()), "CEP");
    
    monitorUI = system.actorOf(Props.create(MonitorUserInterface.class), "MonitorUserInterface");
    
    dataStager = system.actorOf(Props.create(DataStager.class, ods, dwhs, cep, chooseMarketDataFilter()), "DataStager");
  }
  
  private RiskManager chooseRiskManager() {
    return new CreditcheckBasedRiskManager(new SimpleCreditCheck());
  }
  
  private MarketDataFilter chooseMarketDataFilter() {
    Set<String> treatedSymbols = new HashSet<>(Arrays.asList("GOOGL", "ION"));
    return new SymbolBasedMarketDataFilter(treatedSymbols);
  }
  
  private EventProcessingStrategy chooseEventProcessingStrategy() {
    return new TrivialPriceThresholdBasedEventProcessingStrategy();
  }
  
  private void subscribeMonitorUIToEventsAndNotifedOrdersInDataStager() {
    dataStager.tell(new NewEventsSubscription(monitorUI), ActorRef.noSender());
    dataStager.tell(new NotifiedOrdersSubscription(monitorUI), ActorRef.noSender());
  }
  
  private void subscribeDataStagerToExchange() {
    exchange.tell(new Subscription(GateKeeper.ATS_SYSTEM_SUBSCRIBER_ID), dataStager);
  }
  
  private void handleExecutionFlow() {
    System.out.println(">>> >>> Press ENTER to Shut Down ATS System or type Ctrl-C to  abort execution... <<<");
    try {
      System.in.read();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      unsubscribeDataStagerFromExchange();
      logGateways();
      system.terminate();
    }
  }
  
  private void unsubscribeDataStagerFromExchange() {
    exchange.tell(new UnSubscription(GateKeeper.ATS_SYSTEM_SUBSCRIBER_ID), dataStager);
  }
  
  private void logGateways() {
    System.out.println(GateKeeper.eventsGateway);
    System.out.println(GateKeeper.ordersGateway);
    System.out.printf("Wallet Balance is %,.2f%n", GateKeeper.walletGateway.balance());
  }
}
